package com.mdjsmile.eye_coridor.helpers

import spock.lang.Specification

class SpaceConverterTest extends Specification {
    def "bytesToMb"() {
        expect:
        SpaceConverter.bytesToMb(1 * 1024 * 1024 + 1 * 1024 * 1024 / 2 as long) == BigDecimal.valueOf(1.5)
    }
}