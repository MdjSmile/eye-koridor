package com.mdjsmile.eye_coridor.services;

import com.mdjsmile.eye_coridor.managers.GoogleManager;
import com.mdjsmile.eye_coridor.settings.ApplicationParams;
import com.mdjsmile.eye_coridor.settings.WebcamParams;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.opencv.core.*;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.opencv.videoio.VideoCapture;
import org.opencv.videoio.VideoWriter;
import org.opencv.videoio.Videoio;
import org.springframework.stereotype.Service;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

import javax.annotation.PostConstruct;
import javax.swing.*;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Slf4j
@Service
public class WebcamService implements AutoCloseable {
    static {
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
    }

    private static final int FOURCC_MJPG = VideoWriter.fourcc('M', 'J', 'P', 'G');
    private static final int FOCUS_TIME_S = 3;
    private static final int GAUSSIAN_BLUR_W = 5;
    private static final int GAUSSIAN_BLUR_H = 3;
    private static final int THRESH = 25;
    private static final int THRESH_MAX_VAL = 255;
    private static final int DILATE_ITERATIONS = 2;

    private final GoogleManager googleManager;
    private final SimpleDateFormat formatter;
    private final Mat frame;
    private final VideoCapture eye;
    private final Scheduler.Worker worker;
    private final WebcamParams params;
    private final Point anchor;
    private final Mat kernel;
    private final boolean guiMode;
    private final JFrame window;
    private final JLabel imageLabel;

    private VideoWriter videoFile;
    private String fileName;

    public WebcamService(@NonNull GoogleManager googleManager, @NonNull ApplicationParams params) {
        this.googleManager = googleManager;
        this.formatter = new SimpleDateFormat("dd_MM_yyyy_HH_mm_ss");
        this.guiMode = params.isGuiMode();
        this.window = new JFrame("Camera Input Example");
        this.imageLabel = new JLabel();
        this.frame = new Mat();
        this.eye = new VideoCapture();
        this.worker = Schedulers.elastic().createWorker();
        this.params = params.getWebcam();

        final Size kernelSize = new Size(10, 10);
        this.anchor = new Point(-1, -1);
        this.kernel = Imgproc.getStructuringElement(Imgproc.MORPH_DILATE, kernelSize);
    }

    @PostConstruct
    private void init() throws InterruptedException {
        if (guiMode) {
            window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            window.setSize(params.getWidth(), params.getHeight());
            window.add(imageLabel);
            window.setVisible(true);
        }

        eye.set(Videoio.CAP_PROP_FRAME_WIDTH, params.getWidth());
        eye.set(Videoio.CAP_PROP_FRAME_HEIGHT, params.getHeight());
        eye.open(params.getPort());

        File videoFdr = new File("./videosTmp");
        if (!videoFdr.exists() && videoFdr.mkdir()) {
            log.info("Created empty folder videosTmp");
        }

        if (eye.isOpened()) {
            eye.read(frame);
            log.info("Focusing camera {} seconds...", FOCUS_TIME_S);
            TimeUnit.SECONDS.sleep(FOCUS_TIME_S);          // need to focus camera before start
            worker.schedule(this::processReading);
        } else {
            log.error("Failed to open camera");
        }
    }

    private void processReading() {
        try {
            int periodMin = params.getPeriodMin();
            log.info("Started new {} min cycle", periodMin);
            Mat gray;
            Mat firstFrame = null;
            Mat frameDelta = null;
            Mat thresh = null;
            ArrayList<Rect> rects;

            Instant now = Instant.now();
            Instant currentTime = now;
            Size sz = new Size(params.getWidth(), params.getHeight());
            fileName = null;

            while (currentTime.isBefore(now.plus(Duration.ofMinutes(periodMin)))) {
                eye.read(frame);
                if (!frame.empty()) {
                    Imgproc.resize(frame, frame, sz);

                    gray = new Mat(frame.size(), CvType.CV_8UC1);
                    Imgproc.cvtColor(frame, gray, Imgproc.COLOR_BGR2GRAY);
                    Imgproc.GaussianBlur(gray, gray, new Size(GAUSSIAN_BLUR_W, GAUSSIAN_BLUR_H), 0);

                    if (firstFrame == null) {
                        frameDelta = new Mat(gray.size(), CvType.CV_8UC1);
                        thresh = new Mat(gray.size(), CvType.CV_8UC1);
                        firstFrame = gray.clone();
                    } else {
                        Core.absdiff(firstFrame, gray, frameDelta);
                        Imgproc.threshold(frameDelta, thresh, THRESH, THRESH_MAX_VAL, Imgproc.THRESH_BINARY);

                        Imgproc.dilate(thresh, thresh, kernel, anchor, DILATE_ITERATIONS);

                        rects = detection_contours(thresh);
                        final int v1 = 255;
                        for (Rect rect : rects) {
                            Imgproc.rectangle(frame, rect.br(), rect.tl(),
                                    new Scalar(0, v1, 0), 1);
                        }

                        if (rects.size() > 0) {
                            initFile();
                            videoFile.write(frame);
                        }
                    }

                    if (guiMode) {
                        BufferedImage tempImage = toBufferedImage(frame);
                        ImageIcon imageIcon = new ImageIcon(tempImage, "Captured video");
                        imageLabel.setIcon(imageIcon);
                    }
                } else {
                    log.error("Frame from camera not captured!");
                }

                currentTime = Instant.now();
            }

            if (videoFile != null) {
                videoFile.release();
                googleManager.saveFile(fileName);
                videoFile = null;
            }

            log.info("{} min cycle ended", periodMin);
            worker.schedule(this::processReading);
        } catch (Exception e) {
            log.error("Failed to process cam reading: {}", e.getMessage(), e);
            worker.schedule(this::processReading);
        }
    }

    private void initFile() {
        if (videoFile == null) {
            log.info("Video started to record due to movement detection");
            Date date = new Date(System.currentTimeMillis());
            fileName = String.format("./videosTmp/%s.avi", formatter.format(date));
            videoFile = new VideoWriter(fileName, FOURCC_MJPG, params.getFps(),
                    new Size(params.getWidth(), params.getHeight()), true);
        }
    }

    private ArrayList<Rect> detection_contours(Mat image) {
        Mat v = new Mat();
        List<MatOfPoint> contours = new ArrayList<>();
        Imgproc.findContours(image.clone(), contours, v, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);

        double minArea = params.getMinArea();

        ArrayList<Rect> rectArray = new ArrayList<>();

        for (Mat contour : contours) {
            double contourArea = Imgproc.contourArea(contour);
            if (contourArea < minArea) {
                continue;
            }

            Rect rect = Imgproc.boundingRect(contour);
            rectArray.add(rect);
        }

        v.release();

        return rectArray;
    }


    private BufferedImage toBufferedImage(Mat matrix) {
        int type = BufferedImage.TYPE_BYTE_GRAY;
        if (matrix.channels() > 1) {
            type = BufferedImage.TYPE_3BYTE_BGR;
        }
        int bufferSize = matrix.channels() * matrix.cols() * matrix.rows();
        byte[] buffer = new byte[bufferSize];
        matrix.get(0, 0, buffer); // get all the pixels
        BufferedImage image = new BufferedImage(matrix.cols(), matrix.rows(), type);
        final byte[] targetPixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
        System.arraycopy(buffer, 0, targetPixels, 0, buffer.length);
        return image;
    }


    private Mat openFile(String filePath) throws Exception {
        Mat newImage = Imgcodecs.imread(filePath);
        if (newImage.dataAddr() == 0) {
            throw new Exception("Couldn't open file " + filePath);
        }

        return newImage;
    }

    @Override
    public void close() {
        log.info("Closed");
        eye.release();
        if (videoFile != null) {
            videoFile.release();
        }
    }
}