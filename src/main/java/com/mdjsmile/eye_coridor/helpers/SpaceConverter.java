package com.mdjsmile.eye_coridor.helpers;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class SpaceConverter {
    public static final int BYTES_IN_KB = 1024;

    public static BigDecimal bytesToMb(long bytes) {
        BigDecimal defBytes = BigDecimal.valueOf(BYTES_IN_KB);
        return BigDecimal.valueOf(bytes)
                .divide(defBytes, 2, RoundingMode.HALF_UP)
                .divide(defBytes, 2, RoundingMode.HALF_UP);
    }
}