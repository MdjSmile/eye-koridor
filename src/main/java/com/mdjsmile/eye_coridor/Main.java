package com.mdjsmile.eye_coridor;

import com.mdjsmile.eye_coridor.settings.ApplicationParams;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@Slf4j
@SpringBootApplication
@EnableConfigurationProperties({ApplicationParams.class})
public class Main {
    public static void main(String[] args) {
        Thread.setDefaultUncaughtExceptionHandler(Main::uncaughtException);
        System.setProperty("java.awt.headless", "false");
        SpringApplication.run(Main.class, args);
    }

    private static void uncaughtException(Thread thread, Throwable err) {
        log.error("Unhandled exception caught in {}. Message: {}", thread.getName(), err.getMessage(), err);
    }
}