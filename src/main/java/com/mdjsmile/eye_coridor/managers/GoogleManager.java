package com.mdjsmile.eye_coridor.managers;

import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.FileContent;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.model.About;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.FileList;
import com.mdjsmile.eye_coridor.google.DriveQuickstart;
import com.mdjsmile.eye_coridor.helpers.SpaceConverter;
import com.mdjsmile.eye_coridor.settings.ApplicationParams;
import com.mdjsmile.eye_coridor.settings.GoogleParams;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Component;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.List;
import java.util.Properties;

@Slf4j
@Component
public class GoogleManager implements AutoCloseable {
    private static final int SMPT_GMAIL_PORT = 587;
    private static final int FILES_PAGE_SIZE = 500;

    private final Scheduler.Worker worker;

    private final Drive drive;
    private final JavaMailSender emailSender;
    private final GoogleParams params;

    public GoogleManager(@NonNull ApplicationParams params) throws GeneralSecurityException,
            IOException {
        this.params = params.getGoogle();
        final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
        this.drive = new Drive.Builder(HTTP_TRANSPORT, DriveQuickstart.JSON_FACTORY,
                DriveQuickstart.getCredentials(HTTP_TRANSPORT))
                .setApplicationName(params.getName())
                .build();
        this.worker = Schedulers.elastic().createWorker();
        this.emailSender = getJavaMailSender();
    }

    public void saveFile(@NonNull String filename) {
        worker.schedule(() -> processSaveFile(filename));
    }

    @Override
    public void close() {
        log.info("Closed");
    }

    private List<File> getFiles() throws IOException {
        FileList result = drive.files().list()
                .setPageSize(FILES_PAGE_SIZE)
                .setFields("nextPageToken, files(id, name, size)")
                .execute();

        return result.getFiles();
    }

    private void checkAndFreeEmptySpace(java.io.File localFile) throws IOException {
        About.StorageQuota quota = drive.about().get().set("fields", "*").execute().getStorageQuota();
        long freeSpaceBytes = (quota.getLimit() - quota.getUsage()) * SpaceConverter.BYTES_IN_KB;
        long usableSpaceBytes = localFile.getUsableSpace();

        if (usableSpaceBytes > freeSpaceBytes) {
            log.info("Not enough space in google-drive: {} mb, needed: {} mb",
                    SpaceConverter.bytesToMb(freeSpaceBytes), SpaceConverter.bytesToMb(usableSpaceBytes));

            List<File> files = getFiles();

            while (usableSpaceBytes - deleteOldestFile(files) < 0) {
                log.info("File removed from google-drive");
            }
        }
    }

    /**
     * Remove oldest {@link File} from google-drive.
     *
     * @param files - received {@link File} list from google-drive
     * @return restored bytes
     * @throws IOException thrown if no files in google-drive to remove
     */
    private long deleteOldestFile(List<File> files) throws IOException {
        File file = files.stream().reduce((first, second) -> second).orElseThrow();
        long fileSizeBytes = file.getSize() * SpaceConverter.BYTES_IN_KB;
        deleteFile(file);
        files.remove(file);
        log.info("Freed space: {}, file: {}", fileSizeBytes, file);
        return fileSizeBytes;
    }

    private void deleteFile(@NonNull File file) throws IOException {
        log.info("File started to delete from google-drive: {}", file);
        drive.files().delete(file.getId()).execute();
        log.info("File deleted from google-drive: {}", file);
    }

    private void processSaveFile(@NonNull String fileName) {
        log.info("File started to save into google drive: {}", fileName);
        try {
            File metaData = new File();
            metaData.setName(fileName);

            java.io.File localFile = new java.io.File(fileName);
            checkAndFreeEmptySpace(localFile);

            FileContent mediaContent = new FileContent("image/jpeg", localFile);
            File googleFile = drive.files().create(metaData, mediaContent)
                    .setFields("id")
                    .execute();
            log.info("File saved into google drive: {}", fileName);

            if (localFile.delete()) {
                log.info("File deleted from local store: {}", localFile);
            } else {
                log.error("Failed to delete local file: {}", localFile);
            }

            for (String email : params.getSendTo()) {
                sendSimpleMessage(email);
            }
        } catch (Exception e) {
            log.error("Failed to save file into google drive: {}", e.getMessage(), e);
        }
    }

    private void sendSimpleMessage(String to) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(to);
        message.setSubject("Movement detected");
        message.setText("Video saved into google-drive " + params.getUsername());
        emailSender.send(message);
        log.info("Mail sent to {}", to);
    }

    private JavaMailSender getJavaMailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost("smtp.gmail.com");
        mailSender.setPort(SMPT_GMAIL_PORT);

        mailSender.setUsername(params.getUsername());
        mailSender.setPassword(params.getPassword());

        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.debug", "true");

        return mailSender;
    }
}