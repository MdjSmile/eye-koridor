package com.mdjsmile.eye_coridor.settings;

import lombok.Data;

import java.util.List;

@Data
public class GoogleParams {
    private final String username;
    private final String password;
    private final List<String> sendTo;
}