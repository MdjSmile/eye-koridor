package com.mdjsmile.eye_coridor.settings;

import lombok.Data;

@Data
public class WebcamParams {
    private final int width;
    private final int height;
    private final int periodMin;
    private final int fps;
    private final int minArea;
    private final int port;
}