package com.mdjsmile.eye_coridor.settings;

import lombok.Getter;
import lombok.NonNull;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;
import org.springframework.validation.annotation.Validated;

@Getter
@Validated
@ConstructorBinding
@ConfigurationProperties("application")
public class ApplicationParams {
    private final boolean guiMode;
    private final String name;
    private final GoogleParams google;
    private final WebcamParams webcam;

    public ApplicationParams(@NonNull String name, @NonNull GoogleParams google,
                             @NonNull WebcamParams webcam, boolean guiMode) {
        this.name = name;
        this.google = google;
        this.webcam = webcam;
        this.guiMode = guiMode;
    }
}